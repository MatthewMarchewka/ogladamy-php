//funkcja do komunikowania z phpem za pomocą ajaxa
function databaseCommunication(jsonObject) {
    jsonObject = JSON.stringify(jsonObject)
    xhttp = new XMLHttpRequest();
    xhttp.open("POST", "php/curl.php", true)
    xhttp.setRequestHeader("Content-type", "application/json")
    xhttp.send(jsonObject)
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            var data = this.responseText
            if (data) {
                //console.log(data, typeof(data))
                if (data.slice(0, 7) == "addFilm") {
                    console.log("added")
                } else if (data.slice(0, 8) == "getFilms") {
                    var json = JSON.parse(data.slice(8))
                    renderFilms(json, "database")
                } else if (data.slice(0, 10) == "removeFilm") {
                    console.log("removed")
                } else {
                    console.log(data)
                    var data = JSON.parse(data)
                    //var data = JSON.parse(data)
                    //dane zwracane przez stronę
                    var renderData = []
                    for (var i = 0; i < data.length; i++) {
                        if (data[i]["@attributes"].class == "filmPoster__image FilmPoster") {
                            if (data[i + 1].div[0].div[1]) {
                                if (data[i + 1].div[0].div[1].indexOf("min.") == -1) {
                                    renderData.push({
                                        poster: data[i]["@attributes"]["data-src"],
                                        title: data[i + 1].div[0].div[1],
                                        description: data[i + 1].div[2].p,
                                        rating: parseInt(data[i + 1].div[1].div[0].span[0]),
                                        director: "no info",
                                        actors: "no info"
                                    })
                                }
                            }
                        } else if (i > 1 && data[i - 1]["@attributes"].class == "filmPreview__card")
                            if(data[i].div[0].div[1]){
                                if (data[i].div[0].div[1].indexOf("min.") == -1)
                                renderData.push({
                                    poster: "gfx/nophoto.jpg",
                                    title: data[i].div[0].div[1],
                                    description: data[i].div[2].p,
                                    rating: parseInt(data[i].div[1].div[0].span[0]),
                                    director: "No info",
                                    actors: "No info"
                                })
                            }
                    }
                    renderFilms(renderData, "web")
                }
            }
        }
    }
}

function renderFilms(renderData, type) {
    document.getElementById("filmList").innerHTML = ""
    if (renderData.length == 0)
        document.getElementById("filmList").innerHTML = "No <br> Films <br> for <br> you!"
    for (var i = 0; i < renderData.length; i++) {
        var film = new Film(renderData[i].title, renderData[i].poster, renderData[i].description, renderData[i].rating, renderData[i].director, renderData[i].actors, type)
    }
}

function getCookie(name) {
    var value = "; " + document.cookie;
    var parts = value.split("; " + name + "=");
    if (parts.length == 2) return parts.pop().split(";").shift();
}

function init() {
    //navbar load
    document.getElementById("loggedAs").innerHTML += "<b>" + getCookie("userlogin").replace("%40", "@") + "</b>"
    databaseCommunication({ defaultCurl: "true", search: "" })
}

function saved() {
    databaseCommunication({ getFilms: "true", email: getCookie("userloginhashed") })
}

function search() {
    var search = document.getElementById("search").value.replace(" ", "%20")
    var search = search.replace("ł", "l").replace("ą", "a").replace("ę", "e").replace("ć", "c").replace("ż", "z").replace("ź", "z")
    console.log(search)
    databaseCommunication({ defaultCurl: "true", search: search })
}

function logOut() {
    document.cookie = "userlogin=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
    document.cookie = "userloginhashed=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
    window.location.href = "index.html";
}
