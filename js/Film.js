class Film {
    constructor(title, poster, description, rating, director, actors, source) {
        this.title = title
        this.poster = poster
        this.description = description
        this.rating = rating
        this.director = director
        this.actors = actors
        this.source = source
        this.init()
    }
    init() {
        var parent = document.getElementById("filmList")
        //section
        var section = document.createElement("div")
        section.classList.add("filmSection")
        parent.appendChild(section)
        //content
        var content = document.createElement("div")
        content.classList.add("content")
        section.appendChild(content)
        //filmMain
        var filmMain = document.createElement("div")
        filmMain.classList.add("filmMain")
        content.appendChild(filmMain)
        //poster
        var poster = document.createElement("img")
        poster.src = this.poster
        poster.alt = "no photo"
        filmMain.appendChild(poster)
        //filmMainInfo
        var filmMainInfo = document.createElement("div")
        filmMainInfo.classList.add("filmMainInfo")
        filmMain.appendChild(filmMainInfo)
        //title
        var title = document.createElement("h1")
        title.innerHTML = this.title
        filmMainInfo.appendChild(title)
        //info
        var info = document.createElement("div")
        info.classList.add("info")
        filmMainInfo.appendChild(info)
        //director
        // var director = document.createElement("div")
        // director.classList.add("director")
        // director.innerHTML = "Reżyser: "+this.director
        // info.appendChild(director)
        //actors
        // var actors = document.createElement("div")
        // actors.classList.add("director")
        // actors.innerHTML = "Aktorzy: "+this.actors
        // info.appendChild(actors)
        //rating
        var rating = document.createElement("div")
        rating.classList.add("rating")
        rating.innerHTML = "Rating: "
        info.appendChild(rating)
        //stars
        for(var i=0; i<10; i++){
            var star = document.createElement("span")
            star.classList.add("fa")
            star.classList.add("fa-star")
            if(i<this.rating)
                star.classList.add("checked")
            rating.appendChild(star)
        }
        //filmDescription
        var filmDescription = document.createElement("div")
        filmDescription.classList.add("filmDescription")
        content.appendChild(filmDescription)
        //description
        var description = document.createElement("div")
        description.classList.add("description")
        description.innerHTML = this.description
        filmDescription.appendChild(description)
        //buttons
        var buttons = document.createElement("div")
        buttons.classList.add("buttons")
        filmDescription.appendChild(buttons)
        //powered by filmweb
        var poweredBy = document.createElement("span")
        poweredBy.classList.add("navbar-text")
        poweredBy.classList.add("graytext")
        poweredBy.innerHTML = "Powered by: <b>Filmweb</b>"
        buttons.appendChild(poweredBy)
        //functional button
        var functional = document.createElement("button")
        functional.classList.add("btn")
        functional.classList.add("btn-outline-warning")
        functional.classList.add("filmButton")
        functional.onclick = (e)=>{
            if(e.target.innerHTML == "Save"){
                databaseCommunication({ addFilm: "true",
                    email: getCookie("userloginhashed"),
                    title: this.title,
                    description: this.description,
                    rating: this.rating,
                    poster: this.poster
                })
                e.target.innerHTML = "Saved"
            }else if(e.target.innerHTML == "Remove"){
                section.classList.add("hide")
                content.classList.add("remove")
                databaseCommunication({ removeFilm: "true",
                    email: getCookie("userloginhashed"),
                    title: this.title,
                })
                e.target.innerHTML = "Removed"
            }
          }
        if(this.source == "web")
            functional.innerHTML = "Save"
        else
            functional.innerHTML = "Remove"
        buttons.appendChild(functional)
        
    }
}