<?php
    //połącznie z bazą mysql
    $connection = @new mysqli('localhost', 'root','','ogladamydatabase');
    mysqli_set_charset($connection,"utf8");

    $data = json_decode(file_get_contents('php://input'), true);    //pobranie danych z jsona
    
    if(isset($data["defaultCurl"])){
        $connection = curl_init();

        curl_setopt($connection, CURLOPT_URL, "https://www.filmweb.pl/films/search?q=".$data["search"]);
        curl_setopt($connection, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($connection, CURLOPT_SSL_VERIFYPEER, false);
        
        $server_output = curl_exec($connection);

        curl_close($connection);
        $doc = new DOMDocument();
        libxml_use_internal_errors(true);
        $doc->loadHTML($server_output);
        libxml_clear_errors();
        $html = $doc->saveHTML();
        $xml = $doc->loadXML($html);
        if (!$doc){
            print_r('Error while parsing the document');
            exit;
        };
        $xml = simplexml_import_dom($doc);
        $result = $xml->xpath("//div[@class='filmPreview__card'] | //img[@class='filmPoster__image FilmPoster']");
        echo json_encode($result);
    }else if(isset($data["addFilm"])){
        if( empty(@$connection -> query('SELECT * FROM `savedtable` WHERE `title`="'.$data["title"].'" ')->fetch_all(MYSQLI_ASSOC)) ){
            $wynik = @$connection -> query('INSERT INTO `savedtable`(`title`, `description`, `rating`, `poster`) VALUES ("'.$data["title"].'","'.$data["description"].'","'.$data["rating"].'","'.$data["poster"].'")');
        }
        if( empty(@$connection -> query('SELECT * FROM `filmtable` WHERE `email`="'.$data["email"].'" and `title`="'.$data["title"].'" ')->fetch_all(MYSQLI_ASSOC)) ){
            $wynik = @$connection -> query('INSERT INTO `filmtable`(`email`, `title`) VALUES ("' .$data["email"].'","'.$data["title"].'")');
        }
        echo "addFilm";
    }else if(isset($data["getFilms"])){
        $query = mysqli_query($connection, 'SELECT `title` FROM `filmtable` WHERE `email`="'.$data["email"].'" ');
        $films = json_encode($query->fetch_all(MYSQLI_ASSOC));
        $films = json_decode($films);
        $filmsAll = array();
        for ($x = 0; $x < count($films); $x++) {
            $title = $films[$x]->title;
            $queryChild = mysqli_query($connection, 'SELECT * FROM `savedtable` WHERE `title`="'.$title.'" ');
            if(mysqli_num_rows($queryChild)>0){
                while($row = mysqli_fetch_assoc($queryChild)){
                    array_push($filmsAll, $row);
                };
            };
        };
        //$filmsAll = json_encode($filmsAll);
        print_r("getFilms".json_encode($filmsAll));
    }else if(isset($data["removeFilm"])){
        $query = mysqli_query($connection, 'DELETE FROM `filmtable` WHERE `email`="'.$data["email"].'" and `title`="'.$data["title"].'" ');
        print_r("removeFilm");
    };
?>


